FROM amazoncorretto:21-alpine-jdk
VOLUME /tmp
EXPOSE 8080
COPY build/libs/calendarific-API-0.0.1-SNAPSHOT.jar calendarific-API.jar
ENTRYPOINT ["java","-jar","/calendarific-API.jar"]
