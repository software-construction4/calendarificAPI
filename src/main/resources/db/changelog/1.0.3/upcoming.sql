create table upcoming(
    id serial primary key,
    name varchar(255) ,
    description varchar(2048),
    country_name varchar(255),
    date date,
    type varchar(255),
    date_of_access date
);