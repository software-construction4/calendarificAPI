CREATE SEQUENCE country_id_seq;

ALTER TABLE country
    ALTER COLUMN id
        SET DEFAULT NEXTVAL('country_id_seq');