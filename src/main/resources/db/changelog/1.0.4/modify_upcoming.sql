alter table  upcoming
    drop column name,
    drop column description,
    drop column country_name,
    drop column date,
    drop column type,
    drop column date_of_access;

alter table upcoming
    add column holiday_id BIGINT,
    add column month_of_access INTEGER;