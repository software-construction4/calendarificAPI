package org.hse.constraction.calendarificAPI.api;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.hse.constraction.calendarificAPI.client.rest.model.Country;

import java.util.List;

public interface CalendericApi {

    public List<Country> countries();

}
