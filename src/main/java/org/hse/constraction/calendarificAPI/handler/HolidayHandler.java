package org.hse.constraction.calendarificAPI.handler;

import lombok.RequiredArgsConstructor;
import org.hse.constraction.calendarificAPI.client.rest.api.CalendaricClient;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayDTOJson;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayResponse;
import org.hse.constraction.calendarificAPI.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class HolidayHandler {
    private final CalendaricClient calendaricClient;
    @Value("${apiKey}")
    private String apiKey;
    public List<HolidayDTOJson> getHolidayDTOs(String country, Integer year){
        HolidayResponse holidayResponse;
        try{
            holidayResponse = calendaricClient.holidays(apiKey, country, year).response;
        }catch (Exception e){
            throw new NotFoundException();
        }
        return holidayResponse.getHolidayDTOJsons();
    }

    public List<HolidayDTOJson> getHolidayDTOsByDate(String country, Integer year, Integer month, Integer day){
        HolidayResponse holidayResponse;
        try{
            holidayResponse = calendaricClient.holidaysByDate(apiKey, country, year,month,day).response;
        }catch (Exception e){
            throw new NotFoundException();
        }
        return holidayResponse.getHolidayDTOJsons();
    }
}
