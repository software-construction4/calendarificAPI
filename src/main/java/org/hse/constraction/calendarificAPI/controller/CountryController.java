package org.hse.constraction.calendarificAPI.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hse.constraction.calendarificAPI.client.rest.api.CalendaricClient;
import org.hse.constraction.calendarificAPI.client.rest.model.CountriesResponse;
import org.hse.constraction.calendarificAPI.client.rest.model.ResponseJsonTransfer;
import org.hse.constraction.calendarificAPI.client.rest.model.Country;

import org.hse.constraction.calendarificAPI.service.CountryService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Tag(name = "Страны", description = "Возаращает доступные страны")
public class CountryController  {
    private final CalendaricClient calendaricClient;
    private final CountryService countryService;

    @Value("${apiKey}")
    private String apiKey;

    public CountryController(CalendaricClient calendaricClient, CountryService countryService) {
        this.calendaricClient = calendaricClient;
        this.countryService = countryService;
    }

    @GetMapping("/countries")
    public List<Country> countries() {
        List<Country> countries = countryService.getAllCountries();
        if (countries.isEmpty()) {
            ResponseJsonTransfer<CountriesResponse> countriesResponse =  calendaricClient.countries(apiKey);
            countriesResponse.response.countries.forEach(countryService::addCountry);
            return countriesResponse.response.countries;
        }
        return countries;
    }



}
