package org.hse.constraction.calendarificAPI.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.extern.slf4j.Slf4j;
import org.hse.constraction.calendarificAPI.client.rest.api.CalendaricClient;
import org.hse.constraction.calendarificAPI.client.rest.model.Holiday;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayDTOJson;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayResponse;
import org.hse.constraction.calendarificAPI.client.rest.model.UpcomingHoliday;
import org.hse.constraction.calendarificAPI.converter.HolidayConverter;
import org.hse.constraction.calendarificAPI.exceptions.NotFoundException;
import org.hse.constraction.calendarificAPI.handler.HolidayHandler;
import org.hse.constraction.calendarificAPI.service.HolidayService;
import org.hse.constraction.calendarificAPI.service.UpcomingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.function.Consumer;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@Slf4j
@Tag(name = "Праздники", description = "Возаращает доступные празднки")
@Validated
public class HolidayController {
    private final HolidayService holidayService;
    private final UpcomingService upcomingService;
    private final HolidayConverter holidayConverter;

    private final HolidayHandler holidayHandler;
    public HolidayController(HolidayHandler holidayHandler, HolidayService holidayService, UpcomingService upcomingService,HolidayConverter holidayConverter) {
        this.holidayHandler = holidayHandler;
        this.holidayService = holidayService;
        this.upcomingService = upcomingService;
        this.holidayConverter = holidayConverter;
    }

    @GetMapping("/holidays")
    public List<Holiday> holidays(@RequestParam(required = true) String country,
                                       @RequestParam(required = true) Integer year) {
        List<Holiday> holidays = holidayService.getHolidaysByCountryAndYear(country, year);
        if (holidays.isEmpty()){
            List<HolidayDTOJson> holidaysDTOs = holidayHandler.getHolidayDTOs(country, year);
            holidayConverter.convertAndActionHolidaysFromDTOs(holidaysDTOs, holiday -> {
                holidayService.addHoliday(holiday);
                holidays.add(holiday);
            });
        }
        return holidays;
    }

    @GetMapping("/holidays/{name}")
    public Holiday holidays(@RequestParam(required = true) String country,
                                  @RequestParam(required = true) Integer year, @PathVariable("name") String holidayName) throws ChangeSetPersister.NotFoundException {
        AtomicReference<Holiday> holiday = new AtomicReference<>(holidayService.getHolidayByCountryNameAndYearAndHolidayName(country, year, holidayName));

        if (holiday.get() == null) {
            List<HolidayDTOJson> holidaysDTOs = holidayHandler.getHolidayDTOs(country, year);
            holidayConverter.convertAndActionHolidaysFromDTOs(holidaysDTOs, hl -> {
                holidayService.addHoliday(hl);
                if (Objects.equals(hl.getName(), holidayName)){
                    holiday.set(hl);
                }
            });
        }
        if(holiday.get() == null){
            throw new NotFoundException();
        }
        return holiday.get();
    }

    @GetMapping("/upcoming")
    public List<Holiday> upcoming(@RequestParam(required = true) String country) {
        if(upcomingService.checkMonth()){
            upcomingService.clearTable();
        }
        List<Holiday> upcomingHolidays = upcomingService.getUpcomingHolidays(country);
        if (upcomingHolidays.isEmpty()){
            List<Holiday> holidays = holidayService.getHolidaysByCountryAndYear(country, LocalDate.now().getYear());
            if(holidays.isEmpty()){
                List<HolidayDTOJson> holidaysDTOs = holidayHandler.getHolidayDTOs(country, LocalDate.now().getYear());
                holidayConverter.convertAndActionHolidaysFromDTOs(holidaysDTOs, holiday -> {
                    holidayService.addHoliday(holiday);
                    if (holiday.getDate().getMonthValue() == LocalDate.now().getMonthValue()) {
                        upcomingService.addUpcomingHoliday(holiday);
                        if (holiday.getDate().isAfter(LocalDate.now()) || holiday.getDate().isEqual(LocalDate.now()) ){
                            upcomingHolidays.add(holiday);
                        }
                    }
                });

            } else {
                holidays.forEach(holiday -> {
                    if (holiday.getDate().getMonthValue() == LocalDate.now().getMonthValue()) {
                        upcomingService.addUpcomingHoliday(holiday);
                        if (holiday.getDate().isAfter(LocalDate.now()) || holiday.getDate().isEqual(LocalDate.now()) ){
                            upcomingHolidays.add(holiday);
                        }
                    }
                });
            }
        }
        return upcomingHolidays.stream()
                .filter(holiday -> holiday.getDate().isAfter(LocalDate.now()) || holiday.getDate().isEqual(LocalDate.now()))
                .toList();
    }

    @GetMapping("/byType")
    public List<Holiday> holidaysByType(@RequestParam(required = true) String country,
                                        @RequestParam(required = true) Integer year, @RequestParam("type") String type){
        List<Holiday> holidaysByType = holidayService.getHolidaysByType(country, year, type);
        if(holidaysByType.isEmpty()){
            List<HolidayDTOJson> holidaysDTOs = holidayHandler.getHolidayDTOs(country, year);
            holidayConverter.convertAndActionHolidaysFromDTOs(holidaysDTOs, holiday -> {
                holidayService.addHoliday(holiday);
                if (Objects.equals(holiday.getType(), type)){
                    holidaysByType.add(holiday);
                }
            });
        }
        return holidaysByType;
    }

    @GetMapping("/byDate")
    public List<Holiday> holidaysByDate(@RequestParam(required = true) String country,
                                        @RequestParam(required = true) Integer year, @RequestParam("month") @Min(0) @Max(12) Integer month,  @RequestParam("day") @Min(0) @Max(31) Integer day){
        List<Holiday> holidaysByDate = holidayService.getHolidaysByDate(country, year, month,day);
        if(holidaysByDate.isEmpty()){
            List<HolidayDTOJson> holidaysDTOs = holidayHandler.getHolidayDTOsByDate(country, year,month,day);

            holidayConverter.convertAndActionHolidaysFromDTOs(holidaysDTOs, holidaysByDate::add);
        }
        return holidaysByDate;
    }



}
