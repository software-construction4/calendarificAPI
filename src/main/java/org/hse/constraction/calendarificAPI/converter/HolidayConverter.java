package org.hse.constraction.calendarificAPI.converter;

import lombok.RequiredArgsConstructor;
import org.hse.constraction.calendarificAPI.client.rest.model.Holiday;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayDTOJson;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayResponse;
import org.hse.constraction.calendarificAPI.exceptions.NotFoundException;
import org.hse.constraction.calendarificAPI.service.HolidayService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Consumer;

@Component
@RequiredArgsConstructor
public class HolidayConverter {
    private final HolidayService holidayService;
    public void convertAndActionHolidaysFromDTOs(List<HolidayDTOJson> holidaysDTOs, Consumer<Holiday> postProcessAction) {
        holidaysDTOs.forEach(holidayDTOJson -> {
            Holiday holiday = new Holiday(holidayDTOJson);
            holidayService.addHoliday(holiday);
            postProcessAction.accept(holiday);
        });
    }
}
