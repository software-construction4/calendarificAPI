package org.hse.constraction.calendarificAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication

public class CalendarificApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalendarificApiApplication.class, args);
	}

}
