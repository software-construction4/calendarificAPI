package org.hse.constraction.calendarificAPI.configuration;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "org.hse.constraction.calendarificAPI.client.rest.api")
public class FeignClientConfiguration {

}
