package org.hse.constraction.calendarificAPI.repository;

import jakarta.transaction.Transactional;
import org.hse.constraction.calendarificAPI.client.rest.model.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HolidayRepository extends JpaRepository<Holiday,Long> {
    @Transactional
    @Query("select h from Holiday h where h.countryName = ?1 and year(h.date) = ?2")
    List<Holiday> findByCountryNameAndDate(String countryName, Integer year);

    @Transactional
    @Query("select h from Holiday h where  h.countryName = ?1 and year(h.date) = ?2 and h.name = ?3")
    Holiday findByCountryNameAndDateAndHolidayName(String countryName, Integer year, String holidayName);

    @Transactional
    @Query("select h from Holiday h where  h.countryName = ?1 and year(h.date) = ?2 and h.type = ?3")
    List<Holiday> findByCountryAndYearAndType(String countryName, Integer year, String type);

    @Transactional
    @Query("select h from Holiday h where  h.countryName = ?1 and year(h.date) = ?2 and month(h.date) = ?3 and day (h.date) = ?4")
    List<Holiday> findByCountryAndDate(String countryName, Integer year, Integer month, Integer day);
}
