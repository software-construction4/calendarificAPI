package org.hse.constraction.calendarificAPI.repository;

import org.hse.constraction.calendarificAPI.client.rest.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
}
