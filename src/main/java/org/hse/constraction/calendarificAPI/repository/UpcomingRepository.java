package org.hse.constraction.calendarificAPI.repository;


import jakarta.transaction.Transactional;
import org.hse.constraction.calendarificAPI.client.rest.model.Holiday;
import org.hse.constraction.calendarificAPI.client.rest.model.UpcomingHoliday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface UpcomingRepository extends JpaRepository<UpcomingHoliday,Long> {

    @Transactional
    @Query("select h from UpcomingHoliday up join Holiday h on h.id = up.holidayId where h.countryName = ?1 and year(h.date) = ?2 and up.monthOfAccess = ?3")
    List<Holiday> findByCurrentDateAndCountry(String countryName, Integer year, Integer month);


}
