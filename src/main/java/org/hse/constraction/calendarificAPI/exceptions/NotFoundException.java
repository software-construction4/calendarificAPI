package org.hse.constraction.calendarificAPI.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Город не был найден")
public class NotFoundException extends RuntimeException{

}