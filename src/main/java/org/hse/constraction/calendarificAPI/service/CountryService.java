package org.hse.constraction.calendarificAPI.service;

import lombok.extern.slf4j.Slf4j;
import org.hse.constraction.calendarificAPI.client.rest.model.Country;
import org.hse.constraction.calendarificAPI.repository.CountryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CountryService {
    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public void addCountry(Country country){
        countryRepository.save(country);
    }

    public List<Country> getAllCountries(){
        return countryRepository.findAll();
    }
}
