package org.hse.constraction.calendarificAPI.service;

import lombok.AllArgsConstructor;
import org.hse.constraction.calendarificAPI.client.rest.model.Holiday;
import org.hse.constraction.calendarificAPI.client.rest.model.UpcomingHoliday;
import org.hse.constraction.calendarificAPI.repository.UpcomingRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class UpcomingService {
    private final UpcomingRepository upcomingRepository;

    public void addUpcomingHoliday(Holiday holiday){

        upcomingRepository.save(new UpcomingHoliday(holiday.getId(), LocalDate.now().getMonthValue()));
    }

    public List<Holiday> getUpcomingHolidays(String countryName) {
        return upcomingRepository.findByCurrentDateAndCountry(countryName, LocalDate.now().getYear(), LocalDate.now().getMonthValue());
    }
    public void clearTable() {
        upcomingRepository.deleteAll();
    }

    public boolean checkMonth(){
        List<UpcomingHoliday> upcomingHolidays = upcomingRepository.findAll().stream()
                .filter(upcomingHoliday -> upcomingHoliday.getMonthOfAccess().equals(LocalDate.now().getMonthValue()))
                .toList();
        return upcomingHolidays.isEmpty();
    }
}
