package org.hse.constraction.calendarificAPI.service;

import org.hse.constraction.calendarificAPI.client.rest.model.Holiday;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayResponse;
import org.hse.constraction.calendarificAPI.repository.CountryRepository;
import org.hse.constraction.calendarificAPI.repository.HolidayRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HolidayService {
    private final HolidayRepository holidayRepository;

    public HolidayService(HolidayRepository holidayRepository) {
        this.holidayRepository = holidayRepository;
    }
    public void addHoliday(Holiday holiday){
        holidayRepository.save(holiday);
    }

    public List<Holiday> getHolidaysByCountryAndYear(String countryName, Integer year){
        return holidayRepository.findByCountryNameAndDate(countryName, year);
    }
    public Holiday getHolidayByCountryNameAndYearAndHolidayName(String countryName,Integer year, String holidayName){
        return holidayRepository.findByCountryNameAndDateAndHolidayName(countryName,year,holidayName);
    }

    public List<Holiday> getHolidaysByType(String countryName, Integer year, String type){
        return holidayRepository.findByCountryAndYearAndType(countryName, year, type);
    }

    public List<Holiday> getHolidaysByDate(String countryName, Integer year,Integer month, Integer day){
        return holidayRepository.findByCountryAndDate(countryName,year,month,day);
    }
}
