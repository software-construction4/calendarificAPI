package org.hse.constraction.calendarificAPI.client.rest.deserializers;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.hse.constraction.calendarificAPI.client.rest.model.DateDTOJson;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateDeserializer extends JsonDeserializer<DateDTOJson> {

    @Override
    public DateDTOJson deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JacksonException {
        JsonNode node = jp.getCodec().readTree(jp);
        Integer year = (Integer) node.get("datetime").get("year").numberValue();
        Integer month = (Integer) node.get("datetime").get("month").numberValue();
        Integer day = (Integer) node.get("datetime").get("day").numberValue();


        return new DateDTOJson(LocalDate.of(year, month, day));

    }
}
