package org.hse.constraction.calendarificAPI.client.rest.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HolidayResponse {
    @JsonProperty("holidays")
    List<HolidayDTOJson> holidayDTOJsons;
}
