package org.hse.constraction.calendarificAPI.client.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hse.constraction.calendarificAPI.client.rest.deserializers.CountryDeserializer;
import org.hse.constraction.calendarificAPI.client.rest.deserializers.DateDeserializer;

@Data
@AllArgsConstructor
@JsonDeserialize(using = CountryDeserializer.class)
public class CountryDTO {
    @JsonProperty("id")
    private String name;
}
