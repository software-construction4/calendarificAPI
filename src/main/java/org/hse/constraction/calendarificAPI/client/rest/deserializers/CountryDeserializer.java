package org.hse.constraction.calendarificAPI.client.rest.deserializers;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.hse.constraction.calendarificAPI.client.rest.model.CountryDTO;
import org.hse.constraction.calendarificAPI.client.rest.model.DateDTOJson;

import java.io.IOException;
import java.time.LocalDate;


public class CountryDeserializer extends JsonDeserializer<CountryDTO> {

    @Override
    public CountryDTO deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JacksonException {
        JsonNode node = jp.getCodec().readTree(jp);
        String countryId = node.get("id").asText();
        countryId = countryId.toUpperCase();
        return new CountryDTO(countryId);

    }
}