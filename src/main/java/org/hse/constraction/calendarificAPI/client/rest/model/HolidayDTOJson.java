package org.hse.constraction.calendarificAPI.client.rest.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HolidayDTOJson {

    @JsonProperty("name")
    public String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("date")
    private DateDTOJson date;

    @JsonProperty("primary_type")
    private String type;

    @JsonProperty("country")
    private CountryDTO country;

}
