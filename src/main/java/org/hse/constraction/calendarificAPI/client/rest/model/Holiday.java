package org.hse.constraction.calendarificAPI.client.rest.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "holiday")
public class Holiday {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id",columnDefinition = "BIGINT")
    @JsonIgnore
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "country_name")
    private String countryName;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "type")
    private String type;


    public Holiday(HolidayDTOJson dto) {
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.countryName = dto.getCountry().getName();
        this.date = dto.getDate().getDate();
        this.type = dto.getType();
    }
}
