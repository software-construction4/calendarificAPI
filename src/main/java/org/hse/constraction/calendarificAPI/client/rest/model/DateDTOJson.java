package org.hse.constraction.calendarificAPI.client.rest.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hse.constraction.calendarificAPI.client.rest.deserializers.DateDeserializer;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data

@JsonDeserialize(using = DateDeserializer.class)
public class DateDTOJson {
    public DateDTOJson(LocalDate date) {
        this.date = date;
    }
    public LocalDate date;
}
