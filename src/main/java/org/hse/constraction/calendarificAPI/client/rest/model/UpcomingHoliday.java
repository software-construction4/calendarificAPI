package org.hse.constraction.calendarificAPI.client.rest.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "upcoming")
public class UpcomingHoliday {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id",columnDefinition = "BIGINT")
    @JsonIgnore
    private Long id;

    @Column(name = "holiday_id")
    private Long holidayId;

    @Column(name = "month_of_access")
    private Integer monthOfAccess;

    public UpcomingHoliday(Long holidayId, Integer monthOfAccess){
        this.holidayId = holidayId;
        this.monthOfAccess = monthOfAccess;
    }
}
