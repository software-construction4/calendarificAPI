package org.hse.constraction.calendarificAPI.client.rest.api;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hse.constraction.calendarificAPI.client.rest.model.CountriesResponse;
import org.hse.constraction.calendarificAPI.client.rest.model.HolidayResponse;
import org.hse.constraction.calendarificAPI.client.rest.model.ResponseJsonTransfer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "calendarific")
public interface CalendaricClient {

    @GetMapping("/countries")
    ResponseJsonTransfer<CountriesResponse> countries(@RequestParam(name = "api_key") String apiKey);

    @GetMapping("/holidays")
    ResponseJsonTransfer<HolidayResponse> holidays(@RequestParam(name = "api_key") String apiKey,
                                                   @RequestParam(name = "country") String country,
                                                   @RequestParam(name = "year") Integer year);

    @GetMapping("/holidays")
    ResponseJsonTransfer<HolidayResponse> holidaysByDate(@RequestParam(name = "api_key") String apiKey,
                                                         @RequestParam(name = "country") String country,
                                                         @RequestParam(name = "year") Integer year,
                                                         @RequestParam(name = "month") Integer month,
                                                         @RequestParam(name = "day") Integer day );
}